docker buildx   build --platform=linux/amd64   -t yassinekhouaja/workshop:v1.0.0 .

docker push yassinekhouaja/workshop:v1.0.0

minikube start --driver docker --memory 4096 --cpus 4


helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

helm install prometheus prometheus-community/kube-prometheus-stack -n monitoring -f prometheus.yaml --create-namesapce

k port-forward -n monitoring services/prometheus-kube-prometheus-prometheus 9090

k port-forward -n monitoring services/prometheus-grafana 3000:80

k port-forward -n monitoring services/myapp 3001
